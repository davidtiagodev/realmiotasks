# RealmIoTasks

Simple task list project for experiments with Realm.io cloud sync. There is no defined architecture on purpose, the idea was to test what happens when you use only Realm.io functions and classes.
Impressions (so far):
- I was able to setup the Mongo database using only the web tools. Although the process was a bit weird and I know nothing about Mongo setup, no major problems were found, unti later.
- The Realm API for Android feels weird since a lot of things that are considered bad practices in Kotlin are there: multiple method callbacks, mutability, open classes, they are all there.
- I couldn't find any integration with APIs that are well known in Android SDKs like Jetpack libraries, coroutines and everything else. Looks like much code would be required to integrate with those.
- Realm objects, apparently, are made to span across all the layers of the app, meaning that the app will be very dependent from Realm.
- Not sure what is Realm doing under the hood, but there are logs of warnings like `Accessing hidden method` at logcat.
- The Mongo x Realm sync looks very early stage and the project wizard reflects that: weird errors and back and forth between screens.
- The documentation is very confusing, looks incomplete and with no clear sequence of steps.

This project was terminated due to and unexpected limitation: the free tier of Mongo DB cloud is not compatible with Realm Sync. That means it is required to pay for a dedicated instance of the Mong DB Cloud from the day one of the development and experimentation process. Since that is not my intention at all, I dropped the experiment.
