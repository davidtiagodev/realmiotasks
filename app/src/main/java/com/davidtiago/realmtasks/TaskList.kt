package com.davidtiago.realmtasks

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required
import org.bson.types.ObjectId;

open class TaskList(
    @PrimaryKey var _id: ObjectId? = null,
    @Required var checked: Boolean? = null,
    @Required var title: String? = null,
    @Required var user: String? = null
) : RealmObject()
