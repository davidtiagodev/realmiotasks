package com.davidtiago.realmtasks

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.realm.Realm
import io.realm.mongodb.App
import io.realm.mongodb.AppConfiguration
import io.realm.mongodb.Credentials
import io.realm.mongodb.User
import io.realm.mongodb.sync.SyncConfiguration

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Realm.init(this)
        val app = App(
            AppConfiguration.Builder("appId").build()
        )
        val eMail = Credentials.emailPassword("userEMail", "userPassword")
        app.loginAsync(eMail) {
            if (it.isSuccess) {
                Log.d("Login", "Success for user ${app.currentUser()}")

                val user: User = app.currentUser()!!
                val partitionValue: String = "myPartition"
                val config = SyncConfiguration.Builder(user, partitionValue).build()
                val realm: Realm = Realm.getInstance(config)
                Log.d("Realm", "Realm instance created")

                val task = TaskList(
                    checked = false,
                    title = "Title",
                    user = user.name
                )
                realm.executeTransaction { transaction ->
                    transaction.insert(task)
                    transaction.commitTransaction()
                    Log.d("Transaction", "Transaction completed")
                }


            } else {
                Log.d("Login", "Login Error")
            }
        }
    }
}
